// Fill out your copyright notice in the Description page of Project Settings.


#include "ChatWidget.h"
#include "Components/TextBlock.h"
#include "Components/EditableTextBox.h"
#include "Components/Button.h"
#include "ChatClient.h"
#include "Kismet/GameplayStatics.h"

void UChatWidget::NativeConstruct()
{
    Super::NativeConstruct();

    if (SendButton)
    {
        SendButton->OnClicked.AddDynamic(this, &UChatWidget::OnSendButtonClicked);
    }
}

void UChatWidget::OnSendButtonClicked()
{
    if (MessageTextBox)
    {
        FString Message = MessageTextBox->GetText().ToString();
        AChatClient* ChatClient = Cast<AChatClient>(UGameplayStatics::GetActorOfClass(GetWorld(), AChatClient::StaticClass()));
        FString UserNickname = ChatClient->GetNickname();
        if (ChatClient)
        {
            ChatClient->SendMessageToRoom(Message);
        }
        ChatHistory->SetText(FText::FromString(ChatHistory->GetText().ToString() + "\n" + UserNickname + ":" + Message));
        MessageTextBox->SetText(FText::GetEmpty());
    }
}

void UChatWidget::AddMessage(const FString& Message)
{
    if (ChatHistory)
    {
        ChatHistory->SetText(FText::FromString(ChatHistory->GetText().ToString() + "\n" + Message));
    }
}

void UChatWidget::UpdateUserList(const TArray<FString>& Users)
{
    if (UserList)
    {
        FString UserListString;
        for (const FString& User : Users)
        {
            UserListString += User + "\n";
        }
        UserList->SetText(FText::FromString(UserListString));
    }
}