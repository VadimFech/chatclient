// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ChatRoomWidget.generated.h"

/**
 * 
 */
UCLASS()
class CHATCLIENTPROJECT_API UChatRoomWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    virtual void NativeConstruct() override;

    void UpdateRoomList(const TArray<FString>& RoomList);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
    TSubclassOf<UUserWidget> ChatWidgetClass;

protected:
    UPROPERTY(meta = (BindWidget))
    class UEditableTextBox* RoomNameTextBox;

    UPROPERTY(meta = (BindWidget))
    class UButton* CreateRoomButton;

    UPROPERTY(meta = (BindWidget))
    class UButton* JoinRoomButton;

    UPROPERTY(meta = (BindWidget))
    class UTextBlock* RoomListText;

    UFUNCTION()
    void OnCreateRoomButtonClicked();

    UFUNCTION()
    void OnJoinRoomButtonClicked();

private:
    class AChatClient* ChatClient;

    void OpenChatWidget();
};
