// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ChatClientProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHATCLIENTPROJECT_API AChatClientProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
