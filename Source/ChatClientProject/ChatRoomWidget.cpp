// Fill out your copyright notice in the Description page of Project Settings.


#include "ChatRoomWidget.h"
#include "ChatClient.h"
#include "ChatWidget.h"
#include "Components/TextBlock.h"
#include "Components/EditableTextBox.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UChatRoomWidget::NativeConstruct()
{
    Super::NativeConstruct();

    if (CreateRoomButton)
    {
        CreateRoomButton->OnClicked.AddDynamic(this, &UChatRoomWidget::OnCreateRoomButtonClicked);
    }

    if (JoinRoomButton)
    {
        JoinRoomButton->OnClicked.AddDynamic(this, &UChatRoomWidget::OnJoinRoomButtonClicked);
    }

    ChatClient = Cast<AChatClient>(UGameplayStatics::GetActorOfClass(GetWorld(), AChatClient::StaticClass()));
}

void UChatRoomWidget::OnCreateRoomButtonClicked()
{
    if (RoomNameTextBox && ChatClient)
    {
        FString RoomName = RoomNameTextBox->GetText().ToString();
        ChatClient->CreateRoom(RoomName);
    }
}

void UChatRoomWidget::OnJoinRoomButtonClicked()
{
    if (RoomNameTextBox && ChatClient)
    {
        FString RoomName = RoomNameTextBox->GetText().ToString();
        ChatClient->JoinRoom(RoomName);
        OpenChatWidget();
    }
}

void UChatRoomWidget::UpdateRoomList(const TArray<FString>& RoomList)
{
    if (RoomListText)
    {
        FString RoomListString;
        for (const FString& Room : RoomList)
        {
            RoomListString += Room + "\n";
        }
        RoomListText->SetText(FText::FromString(RoomListString));
    }
}

void UChatRoomWidget::OpenChatWidget()
{
    if (ChatWidgetClass)
    {
        UUserWidget* ChatWidget = CreateWidget<UUserWidget>(GetWorld(), ChatWidgetClass);
        if (ChatWidget)
        {
            ChatWidget->AddToViewport();
            ChatClient->SetChatWidget(Cast<UChatWidget>(ChatWidget));
            RemoveFromParent();
        }
    }
}