// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "ChatClient.generated.h"

UCLASS()
class CHATCLIENTPROJECT_API AChatClient : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChatClient();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    void EndPlay(const EEndPlayReason::Type EndPlayReason);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    void CreateRoom(const FString& RoomName);
    void JoinRoom(const FString& RoomName);
    
    // ������� ��� ����������� � �������
    bool ConnectToServer(const FString& ServerAddress, int32 ServerPort);

    // ������� ��� �������� ��������� �� ������
    void SendMessageToServer(const FString& Message);

    //������� ��� �������� ��������� � �������
    void SendMessageToRoom(const FString& Message);

    // ������� ��� ��������� ���������, ���������� � �������
    void ReceiveMessageFromServer();

    // ������� ��� ��������� ���� �� �������
    void SetNickname(const FString& InNickname);

    FString GetNickname();

    void SetChatWidget(class UChatWidget* InChatWidget);
    void SetChatRoomWidget(class UChatRoomWidget* InChatRoomWidget);

    // ������� ��� ���������� �� �������
    void DisconnectFromServer();
private:
    // ����� ��� ����������� �������
    FSocket* ClientSocket;
    FString Nickname;
    FString CurrentRoom;

    UPROPERTY()
    class UChatWidget* ChatWidget;

    UPROPERTY()
    class UChatRoomWidget* ChatRoomWidget;
};
