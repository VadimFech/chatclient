// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ConnectWidget.generated.h"

/**
 * 
 */
UCLASS()
class CHATCLIENTPROJECT_API UConnectWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    virtual void NativeConstruct() override;

protected:
    UPROPERTY(meta = (BindWidget))
    class UEditableTextBox* NicknameTextBox;

    UPROPERTY(meta = (BindWidget))
    class UEditableTextBox* ServerAddressTextBox;

    UPROPERTY(meta = (BindWidget))
    class UEditableTextBox* ServerPortTextBox;

    UPROPERTY(meta = (BindWidget))
    class UButton* ConnectButton;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
    TSubclassOf<UUserWidget> ChatRoomWidgetClass;

private:
    UFUNCTION()
    void OnConnectButtonClicked();

};
