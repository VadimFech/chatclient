// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ChatWidget.generated.h"

/**
 * 
 */
UCLASS()
class CHATCLIENTPROJECT_API UChatWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    virtual void NativeConstruct() override;

    UPROPERTY(meta = (BindWidget))
    class UTextBlock* ChatHistory;

    UPROPERTY(meta = (BindWidget))
    class UTextBlock* UserList;

    UPROPERTY(meta = (BindWidget))
    class UEditableTextBox* MessageTextBox;

    UPROPERTY(meta = (BindWidget))
    class UButton* SendButton;

    UFUNCTION()
    void OnSendButtonClicked();

    UFUNCTION(BlueprintCallable, Category = "Chat")
    void AddMessage(const FString& Message);

    UFUNCTION(BlueprintCallable, Category = "Chat")
    void UpdateUserList(const TArray<FString>& Users);
};
