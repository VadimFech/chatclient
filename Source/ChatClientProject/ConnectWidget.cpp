// Fill out your copyright notice in the Description page of Project Settings.


#include "ConnectWidget.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "ChatClient.h"
#include "Kismet/GameplayStatics.h"
#include "ChatRoomWidget.h"

void UConnectWidget::NativeConstruct()
{
    Super::NativeConstruct();

    if (ConnectButton)
    {
        ConnectButton->OnClicked.AddDynamic(this, &UConnectWidget::OnConnectButtonClicked);
    }
}

void UConnectWidget::OnConnectButtonClicked()
{
    if (NicknameTextBox && ServerAddressTextBox && ServerPortTextBox)
    {
        FString Nickname = NicknameTextBox->GetText().ToString();
        FString ServerAddress = ServerAddressTextBox->GetText().ToString();
        int32 ServerPort = FCString::Atoi(*ServerPortTextBox->GetText().ToString());

        UE_LOG(LogTemp, Warning, TEXT("Attempting to connect to server at %s:%d with Nickname: %s"), *ServerAddress, ServerPort, *Nickname);

        AChatClient* ChatClient = Cast<AChatClient>(UGameplayStatics::GetActorOfClass(GetWorld(), AChatClient::StaticClass()));
        if (ChatClient)
        {
            ChatClient->SetNickname(Nickname);
            if (ChatClient->ConnectToServer(ServerAddress, ServerPort))
            {
                RemoveFromParent();

                if (ChatRoomWidgetClass)
                {
                    UUserWidget* ChatRoomWidget = CreateWidget<UUserWidget>(GetWorld(), ChatRoomWidgetClass);
                    if (ChatRoomWidget)
                    {
                        ChatRoomWidget->AddToViewport();
                        ChatClient->SetChatRoomWidget(Cast<UChatRoomWidget>(ChatRoomWidget));
                    }
                }
            }
        }
    }
}