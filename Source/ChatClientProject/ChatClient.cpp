// Fill out your copyright notice in the Description page of Project Settings.

#include "ChatClient.h"
#include "ChatWidget.h"
#include "ChatRoomWidget.h"
#include "Engine/Engine.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Networking/Public/Networking.h"
#include "IPAddress.h"
#include "Networking/Public/Interfaces/IPv4/IPv4Address.h"

// ������������� �������� �� ���������
AChatClient::AChatClient()
{
    PrimaryActorTick.bCanEverTick = true;
    ClientSocket = nullptr;
}

// ���������� ��� ������� ���� ��� ��� �������� ������
void AChatClient::BeginPlay()
{
    Super::BeginPlay();
}

void AChatClient::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    DisconnectFromServer();
    Super::EndPlay(EndPlayReason);
}

// ���������� ������ ����
void AChatClient::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (ClientSocket)
    {
        uint32 PendingDataSize = 0;
        if (ClientSocket->HasPendingData(PendingDataSize))
        {
            ReceiveMessageFromServer();
        }
    }
}

bool AChatClient::ConnectToServer(const FString& ServerAddress, int32 ServerPort)
{
    ClientSocket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);

    if (!ClientSocket)
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to create client socket"));
        return false;
    }

    TSharedRef<FInternetAddr> InternetAddr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
    bool bIsValid;
    InternetAddr->SetIp(*ServerAddress, bIsValid);
    InternetAddr->SetPort(ServerPort);

    if (!bIsValid)
    {
        UE_LOG(LogTemp, Error, TEXT("Invalid IP address"));
        return false;
    }

    bool bConnected = ClientSocket->Connect(*InternetAddr);

    if (!bConnected)
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to connect to server"));
        return false;
    }

    UE_LOG(LogTemp, Warning, TEXT("Connected to server"));

    // �������� ���� �� ������ ����� �����������
    if (!Nickname.IsEmpty())
    {
        SendMessageToServer(Nickname);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("Nickname is empty"));
    }

    // ������ ������ ������ ����� �����������
    FPlatformProcess::Sleep(0.5f);
    SendMessageToServer(TEXT("REQUEST_ROOMLIST"));
    UE_LOG(LogTemp, Warning, TEXT("Requesting room list from server"));

    return true;
}

void AChatClient::SendMessageToServer(const FString& Message)
{
    if (ClientSocket && ClientSocket->GetConnectionState() == SCS_Connected)
    {
        const TCHAR* SerializedChar = Message.GetCharArray().GetData();
        int32 Size = FCString::Strlen(SerializedChar) + 1;
        int32 Sent = 0;

        ClientSocket->Send((uint8*)TCHAR_TO_UTF8(SerializedChar), Size, Sent);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to send message: Not connected to server"));
    }
}

void AChatClient::ReceiveMessageFromServer()
{
    TArray<uint8> ReceivedData;
    uint32 Size;
    while (ClientSocket->HasPendingData(Size))
    {
        ReceivedData.SetNumUninitialized(FMath::Min(Size, 65507u));

        int32 Read = 0;
        ClientSocket->Recv(ReceivedData.GetData(), ReceivedData.Num(), Read);

        FString ReceivedString = FString(UTF8_TO_TCHAR(ReceivedData.GetData()));
        UE_LOG(LogTemp, Warning, TEXT("Message from server: %s"), *ReceivedString);

        // ��������, �������� �� ��������� ������� �������������
        if (ReceivedString.StartsWith(TEXT("USERLIST:")))
        {
            FString UserListString = ReceivedString.RightChop(9); // ������� "USERLIST:"
            TArray<FString> UserList;
            UserListString.ParseIntoArray(UserList, TEXT(","), true);

            // ���������� ������ ������������� � �������
            if (ChatWidget)
            {
                ChatWidget->UpdateUserList(UserList);
            }
        }
        // ��������, �������� �� ��������� ������� ������
        else if (ReceivedString.StartsWith(TEXT("ROOMLIST:")))
        {
            FString RoomListString = ReceivedString.RightChop(9); // ������� "ROOMLIST:"
            TArray<FString> RoomList;
            RoomListString.ParseIntoArray(RoomList, TEXT(","), true);

            // ���������� ������ ������ � �������
            if (ChatRoomWidget)
            {
                ChatRoomWidget->UpdateRoomList(RoomList);
            }
        }
        else
        {
            // �������� ��������� � ChatWidget
            if (ChatWidget)
            {
                ChatWidget->AddMessage(ReceivedString);
            }
        }
    }
}

void AChatClient::SetNickname(const FString& InNickname)
{
    Nickname = InNickname;
}

FString AChatClient::GetNickname()
{
    return FString(Nickname);
}

void AChatClient::SetChatWidget(UChatWidget* InChatWidget)
{
    ChatWidget = InChatWidget;
}

void AChatClient::SetChatRoomWidget(UChatRoomWidget* InChatRoomWidget)
{
    ChatRoomWidget = InChatRoomWidget;
}

void AChatClient::DisconnectFromServer()
{
    if (ClientSocket && ClientSocket->GetConnectionState() == SCS_Connected)
    {
        FString DisconnectMessage = TEXT("DISCONNECT:") + Nickname;
        SendMessageToServer(DisconnectMessage);
        ClientSocket->Close();
        ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(ClientSocket);
        ClientSocket = nullptr;
    }
}

void AChatClient::CreateRoom(const FString& RoomName)
{
    if (ClientSocket && ClientSocket->GetConnectionState() == SCS_Connected)
    {
        FString Message = FString::Printf(TEXT("CREATE_ROOM:%s"), *RoomName);
        SendMessageToServer(Message);
    }
}

void AChatClient::JoinRoom(const FString& RoomName)
{
    if (ClientSocket && ClientSocket->GetConnectionState() == SCS_Connected)
    {
        FString Message = FString::Printf(TEXT("JOIN_ROOM:%s"), *RoomName);
        SendMessageToServer(Message);
        CurrentRoom = RoomName; // ��������� ������� �������

        // ���������� ������ �� ��������� ������ ������������� � �������
        FPlatformProcess::Sleep(2.f);
        FString RequestUserListMessage = FString::Printf(TEXT("REQUEST_USERLIST:%s"), *RoomName);
        SendMessageToServer(RequestUserListMessage);
    }
}

void AChatClient::SendMessageToRoom(const FString& Message)
{
    if (ClientSocket && ClientSocket->GetConnectionState() == SCS_Connected && !CurrentRoom.IsEmpty())
    {
        FString FullMessage = FString::Printf(TEXT("%s:%s"), *Nickname, *Message);
        SendMessageToServer(FullMessage);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to send message: Not connected to server or not in a room"));
    }
}